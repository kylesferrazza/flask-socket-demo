import mimetypes
import base64
from flask import Flask, render_template, request, redirect, make_response
from flask_socketio import SocketIO

app = Flask(__name__)
socketio = SocketIO(app)

GLOBALS = {
  'name': 'Demo Name',
  'age': 23,
  'images': {
    'image_1': None,
  },
}

@app.route("/display")
def display():
  return make_response(render_template('display.html', **GLOBALS))

@app.route("/config", methods=['GET'])
def config():
  return render_template('config.html', **GLOBALS)

@app.route("/config", methods=['POST'])
def do_config():
  global GLOBALS
  for key, val in request.form.items():
    GLOBALS[key] = val
  for filename, file in request.files.items():
    file_contents = file.read()
    if len(file_contents) == 0:
      continue
    GLOBALS[filename] = base64.b64encode(file_contents).decode('utf-8')
    GLOBALS[f"{filename}_mime"] = mimetypes.guess_type(file.filename)[0]
  socketio.emit("refresh")
  return redirect("/config")

@app.context_processor
def field_processor():
  def text_field(display_name: str, field_name: str, field_value: str):
    return render_template(
      'fields/text.html',
      display_name=display_name,
      field_name=field_name,
      field_value=field_value,
    )
  return dict(
    text_field=text_field,
  )
